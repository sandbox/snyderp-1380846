Search Exclude
===

About
---

A module to exclude individual nodes from the search system.  Nodes are
excluded from the search results at search / query time, so excluded nodes
are still indexed.  This allows for instant inclusion / exclusion of the
nodes from the search results.

Usage
---

Enabling this module will create a new fieldset on each node's form.  The 
fieldset is titled "Search Exclude" and contains one checkbox.  Checking this
box and saving the node will cause the node to be excluded from the core search
system.  Unchecking and re-saving the node will cause the node to be included
in the search results again.

 - snyderp <snyderp@gmail.com>